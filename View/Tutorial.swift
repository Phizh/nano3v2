//
//  Tutorial.swift
//  nano3v2 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 23/08/21.
//

import Foundation
import SwiftUI

struct TutorialView: View {
    var body: some View {
        VStack{
            Text("Go Dungeon!")
                .padding()
        }
    }
}

struct TutorialView_Previews: PreviewProvider {
    static var previews: some View {
        TutorialView()
    }
}
