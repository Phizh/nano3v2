//
//  Backpack.swift
//  nano3v2 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 27/08/21.
//

import Foundation
import SwiftUI

struct BackpackView: View {
   //@ObservedObject var globalString: GlobalString
    @Binding var bag:Bool
    @StateObject var globalString: GlobalString
    var body: some View {
        VStack{
            Text("Current Potion \(globalString.potNum)")
                .padding()
            Text("My Stats : ")
                .padding()
            Text("Lv : \(globalString.myLv) ")
            Text("HP : \(globalString.myHP.description)/\(globalString.myMaxHP.description) ")
            Text("Atk : \(globalString.myAtk.description) ")
        }
       
    }
}
