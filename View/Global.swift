//
//  Global.swift
//  nano3v2 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 25/08/21.
//

import Foundation
import SwiftUI

class GlobalString: ObservableObject {
  @Published var myHP:Double = 100
  @Published var myMaxHP:Double = 100
  @Published var enyHP:Double = 100
  @Published var enyMaxHP:Double = 100
  @Published var myAtk:Double = 20
  @Published var enyAtk:Double = 10
  @Published var potNum:Int = 3
  @Published var turn:Bool = true
  @Published var myLv:Int = 1
}
