//
//  Battle.swift
//  nano3v2 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 24/08/21.
//

import Foundation
import SwiftUI

struct BattleView: View {
    @Binding var event:Bool
    @StateObject var globalString: GlobalString
    
    var body: some View {
        VStack{
            Text("Enemy HP")
                .position(x:80,y:30)
                .frame(width: 100, height: 20)
                .fixedSize()
            ProgressView(value: globalString.enyHP, total: globalString.enyMaxHP)
                .position(x:80,y:30)
                .frame(width: 125, height: 17)
            HStack{
                Button("attack") {
                    if (globalString.turn) {
                        if ((globalString.enyHP - globalString.myAtk) <= 0) {
                            globalString.enyMaxHP += 20
                            globalString.enyHP = globalString.enyMaxHP
                            globalString.enyAtk += 3
                            
                            globalString.myMaxHP += 20
                            globalString.myAtk += 5
                            globalString.myLv += 1
                            event.toggle()
                        } else {
                            globalString.enyHP -= globalString.myAtk
                            //globalString.turn.toggle()
                            globalString.myHP -= globalString.enyAtk
                        }
                    }
                }
                Button("Heal") {
                    if (globalString.turn) {
                        if ((globalString.myHP + 100) > globalString.myMaxHP) {
                            globalString.myHP = globalString.myMaxHP
                        } else {
                            globalString.myHP += 100
                        }
                        globalString.potNum -= 1
                    }
                }
            }
            .position(x: 85, y: 55)
            .frame(width: 175, height: 50)
            
            Text("My Hp Lv: \(globalString.myLv)")
                .frame(width: 100, height: 20)
                .position(x: -25, y: 50)
                .fixedSize()
            ProgressView(value: globalString.myHP, total: globalString.myMaxHP)
                .position(x:40,y:50)
                .frame(width: 125, height: 17)
        }
        .position(x:95,y:50)
    }
}

//struct BattleView_Previews: PreviewProvider {
//    let globalString:GlobalString
//    static var previews: some View {
//        BattleView(globalString: globalString)
//    }
//}
