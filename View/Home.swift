//
//  Home.swift
//  nano3v2 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 23/08/21.
//

import Foundation
import SwiftUI

struct HomeView: View {
   //@ObservedObject var globalString: GlobalString
    var body: some View {
        VStack{
            Text("Go Dungeon!")
                .padding()
            NavigationLink(destination: ChoiceView()) {
                Text("Start Game")
                    .padding()
            }
            NavigationLink(destination: TutorialView()) {
                Text("How to Play")
                    .padding()
            }
        }
       
    }
}

//struct HomeView_Previews: PreviewProvider {
//    static var previews: some View {
//        HomeView()
//    }
//}
