//
//  Treasure.swift
//  nano3v2 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 25/08/21.
//

import Foundation
import SwiftUI

struct TreasureView: View {
    @Binding var event:Bool
    @StateObject var globalString: GlobalString
    var body: some View {
        VStack{
            Image("treasure")
                .resizable()
                .position(x:40,y:40)
                .aspectRatio(contentMode: .fit)
                .frame(width: 80, height: 80)
            Button("Open Chest") {
                randomChest()
                event.toggle()
            }
        }
    }
    
    func randomChest() {
        var ang = Int.random(in: 1..<3)
        switch ang {
        case 1:
            globalString.potNum += Int.random(in: 1..<3)
        case 2:
            globalString.myHP -= 20
        default:
            print("Kamu tidak seharusnya lihat ini")
        }
    }
}

//struct TreasureView_Previews: PreviewProvider {
//    static var previews: some View {
//        TreasureView()
//    }
//}
