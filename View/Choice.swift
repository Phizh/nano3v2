//
//  Choice.swift
//  nano3v2 WatchKit Extension
//
//  Created by Vincentius Phillips Zhuputra on 23/08/21.
//

import Foundation
import SwiftUI

struct ChoiceView: View {
    @State var event = false
    @State var bag = false
    @State private var globalString: GlobalString = GlobalString()
    @State private var showBattle = false
    @State private var showTreasure = false
    @State private var showStore = false
    var body: some View {
        VStack{
            HStack {
                NavigationLink(destination: randomEvent(), isActive: $event) {
                    Text("Go Left")
                }
                NavigationLink(destination: randomEvent(), isActive: $event) {
                    Text("Go Right")
                }
            }
            NavigationLink(destination: BackpackView(bag: $bag, globalString: globalString), isActive: $bag) {
                Text("Backpack")
            }
        }
    }
    func randomEvent() -> AnyView {
        var dest:AnyView! //= AnyView(BattleView(event: $event, globalString: globalString))
        var ang = Int.random(in: 1..<3)
        switch ang {
        case 1:
            dest = AnyView(BattleView(event: $event, globalString: globalString))
        case 2:
            dest = AnyView(TreasureView(event: $event, globalString: globalString))
        case 3:
            dest = AnyView(TreasureView(event: $event, globalString: globalString))
        default:
            print("Kamu tidak seharusnya lihat ini")
        }
        return dest
    }
}

//struct ChoiceView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChoiceView(globalString: <#GlobalString#>)
//    }
//}
